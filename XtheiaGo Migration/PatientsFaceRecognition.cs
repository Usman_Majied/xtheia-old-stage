﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PatientsFaceRecognition
    {
        public int Id { get; set; }
        public int PatientId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string OrginalImagePath { get; set; }
        public string SourceFolder { get; set; }
        public string OrginalImage { get; set; }
        public string ImageBinding { get; set; }

        public virtual Patients Patient { get; set; }
    }
}
