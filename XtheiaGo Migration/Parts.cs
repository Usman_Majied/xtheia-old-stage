﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Parts
    {
        public int PartId { get; set; }
        public int PartTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime? FirmwareDate { get; set; }
        public string SerialNo { get; set; }
        public int? DeviceId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int PartLocationId { get; set; }

        public virtual PartLocations PartLocation { get; set; }
        public virtual PartTypes PartType { get; set; }
    }
}
