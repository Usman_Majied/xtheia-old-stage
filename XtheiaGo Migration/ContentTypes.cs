﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class ContentTypes
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
