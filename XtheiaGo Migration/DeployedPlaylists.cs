﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class DeployedPlaylists
    {
        public int PlaylistId { get; set; }
        public int DeviceId { get; set; }
        public DateTime? ReleaseDate { get; set; }
        public int DeployedPlaylistId { get; set; }
        public sbyte IsDeployed { get; set; }

        public virtual Playlists Playlist { get; set; }
    }
}
