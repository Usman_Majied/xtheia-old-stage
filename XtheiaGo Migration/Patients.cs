﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Patients
    {
        public Patients()
        {
            PatientsFaceRecognition = new HashSet<PatientsFaceRecognition>();
            PhysicianPatients = new HashSet<PhysicianPatients>();
        }

        public int PatientId { get; set; }
        public string PatientNo { get; set; }
        public string Name { get; set; }
        public DateTime Dob { get; set; }
        public string Ssn { get; set; }
        public string Email { get; set; }

        public virtual ICollection<PatientsFaceRecognition> PatientsFaceRecognition { get; set; }
        public virtual ICollection<PhysicianPatients> PhysicianPatients { get; set; }
    }
}
