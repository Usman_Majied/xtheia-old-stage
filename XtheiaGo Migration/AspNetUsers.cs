﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class AspNetUsers
    {
        public AspNetUsers()
        {
            AspNetUserClaims = new HashSet<AspNetUserClaims>();
            AspNetUserLogins = new HashSet<AspNetUserLogins>();
            AspNetUserRoles = new HashSet<AspNetUserRoles>();
            AspNetUserTokens = new HashSet<AspNetUserTokens>();
            Contents = new HashSet<Contents>();
            Devices = new HashSet<Devices>();
            PhysicianPatients = new HashSet<PhysicianPatients>();
            Playlists = new HashSet<Playlists>();
            UsersSecurityQuestions = new HashSet<UsersSecurityQuestions>();
        }

        public long Id { get; set; }
        public string UserName { get; set; }
        public string NormalizedUserName { get; set; }
        public string Email { get; set; }
        public string NormalizedEmail { get; set; }
        public int EmailConfirmed { get; set; }
        public string PasswordHash { get; set; }
        public string SecurityStamp { get; set; }
        public string ConcurrencyStamp { get; set; }
        public string PhoneNumber { get; set; }
        public int PhoneNumberConfirmed { get; set; }
        public int TwoFactorEnabled { get; set; }
        public DateTime? LockoutEnd { get; set; }
        public int LockoutEnabled { get; set; }
        public int AccessFailedCount { get; set; }
        public int? AccountId { get; set; }
        public int? StatusId { get; set; }
        public int? TitleId { get; set; }
        public string GoogleAccessToken { get; set; }
        public string AvatarPath { get; set; }
        public string OneDriveToken { get; set; }
        public string DropboxAccessToken { get; set; }
        public string ConnectState { get; set; }
        public DateTime? CreateAt { get; set; }
        public DateTime? UpdateAt { get; set; }
        public string Name { get; set; }

        public virtual Accounts Account { get; set; }
        public virtual Titles Title { get; set; }
        public virtual ICollection<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual ICollection<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual ICollection<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual ICollection<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual ICollection<Contents> Contents { get; set; }
        public virtual ICollection<Devices> Devices { get; set; }
        public virtual ICollection<PhysicianPatients> PhysicianPatients { get; set; }
        public virtual ICollection<Playlists> Playlists { get; set; }
        public virtual ICollection<UsersSecurityQuestions> UsersSecurityQuestions { get; set; }
    }
}
