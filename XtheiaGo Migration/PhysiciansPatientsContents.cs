﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysiciansPatientsContents
    {
        public PhysiciansPatientsContents()
        {
            PhysiciansPatientsContentsVersion = new HashSet<PhysiciansPatientsContentsVersion>();
        }

        public int Id { get; set; }
        public sbyte? SharedGlobally { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int PatientContentTypeId { get; set; }
        public int PhysicianPatientId { get; set; }
        public string Description { get; set; }
        public string BodyParts { get; set; }
        public int? PhysicialFileId { get; set; }

        public virtual PatientContentTypes PatientContentType { get; set; }
        public virtual PhysicialFiles PhysicialFile { get; set; }
        public virtual PhysicianPatients PhysicianPatient { get; set; }
        public virtual ICollection<PhysiciansPatientsContentsVersion> PhysiciansPatientsContentsVersion { get; set; }
    }
}
