﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysicialFiles
    {
        public PhysicialFiles()
        {
            Contents = new HashSet<Contents>();
            PhysicialFileSchedulerActions = new HashSet<PhysicialFileSchedulerActions>();
            PhysicialSubFiles = new HashSet<PhysicialSubFiles>();
            PhysiciansPatientsContents = new HashSet<PhysiciansPatientsContents>();
            PhysiciansPatientsContentsVersion = new HashSet<PhysiciansPatientsContentsVersion>();
        }

        public int Id { get; set; }
        public string Filename { get; set; }
        public string Ext { get; set; }
        public string EncodedPath { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public TimeSpan Length { get; set; }
        public string ExtraInfo { get; set; }
        public int? PreviousMergeLocation { get; set; }

        public virtual ICollection<Contents> Contents { get; set; }
        public virtual ICollection<PhysicialFileSchedulerActions> PhysicialFileSchedulerActions { get; set; }
        public virtual ICollection<PhysicialSubFiles> PhysicialSubFiles { get; set; }
        public virtual ICollection<PhysiciansPatientsContents> PhysiciansPatientsContents { get; set; }
        public virtual ICollection<PhysiciansPatientsContentsVersion> PhysiciansPatientsContentsVersion { get; set; }
    }
}
