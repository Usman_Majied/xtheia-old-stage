﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Titles
    {
        public Titles()
        {
            AccountsTitle = new HashSet<Accounts>();
            AccountsTitleIdSecondaryNavigation = new HashSet<Accounts>();
            AspNetUsers = new HashSet<AspNetUsers>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Accounts> AccountsTitle { get; set; }
        public virtual ICollection<Accounts> AccountsTitleIdSecondaryNavigation { get; set; }
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
    }
}
