﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PatientContentTypes
    {
        public PatientContentTypes()
        {
            PhysiciansPatientsContents = new HashSet<PhysiciansPatientsContents>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PhysiciansPatientsContents> PhysiciansPatientsContents { get; set; }
    }
}
