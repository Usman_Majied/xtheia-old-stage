﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class StatusTypes
    {
        public StatusTypes()
        {
            Accounts = new HashSet<Accounts>();
            Contents = new HashSet<Contents>();
            Devices = new HashSet<Devices>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Accounts> Accounts { get; set; }
        public virtual ICollection<Contents> Contents { get; set; }
        public virtual ICollection<Devices> Devices { get; set; }
    }
}
