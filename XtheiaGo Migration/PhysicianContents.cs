﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysicianContents
    {
        public int ContentId { get; set; }
        public long PhysicianId { get; set; }

        public virtual Contents Content { get; set; }
    }
}
