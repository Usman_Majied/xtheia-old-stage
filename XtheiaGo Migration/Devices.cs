﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Devices
    {
        public int DeviceId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Sku { get; set; }
        public long? AssignedUserId { get; set; }
        public int StatusId { get; set; }
        public int DeviceTypeId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual AspNetUsers AssignedUser { get; set; }
        public virtual DeviceTypes DeviceType { get; set; }
        public virtual StatusTypes Status { get; set; }
    }
}
