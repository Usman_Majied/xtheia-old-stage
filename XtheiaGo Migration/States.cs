﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class States
    {
        public States()
        {
            Accounts = new HashSet<Accounts>();
        }

        public int Id { get; set; }
        public string Abbr { get; set; }
        public string Name { get; set; }
        public int CountryId { get; set; }

        public virtual Countries Country { get; set; }
        public virtual ICollection<Accounts> Accounts { get; set; }
    }
}
