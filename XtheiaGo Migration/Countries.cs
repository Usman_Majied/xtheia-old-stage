﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Countries
    {
        public Countries()
        {
            Accounts = new HashSet<Accounts>();
            States = new HashSet<States>();
        }

        public int Id { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }

        public virtual ICollection<Accounts> Accounts { get; set; }
        public virtual ICollection<States> States { get; set; }
    }
}
