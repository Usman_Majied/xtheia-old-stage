﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Contents
    {
        public Contents()
        {
            PhysicianContents = new HashSet<PhysicianContents>();
            PlaylistContents = new HashSet<PlaylistContents>();
        }

        public int ContentId { get; set; }
        public sbyte? SharedGlobally { get; set; }
        public int? TypeId { get; set; }
        public long OwnerId { get; set; }
        public string ProductCatagory { get; set; }
        public string Description { get; set; }
        public int CatagoryId { get; set; }
        public sbyte? PreimumStatus { get; set; }
        public int StatusId { get; set; }
        public float RevisionNumber { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? BackgroundRemoval { get; set; }
        public int PhysicialFileId { get; set; }

        public virtual ContentCatagories Catagory { get; set; }
        public virtual AspNetUsers Owner { get; set; }
        public virtual PhysicialFiles PhysicialFile { get; set; }
        public virtual StatusTypes Status { get; set; }
        public virtual ICollection<PhysicianContents> PhysicianContents { get; set; }
        public virtual ICollection<PlaylistContents> PlaylistContents { get; set; }
    }
}
