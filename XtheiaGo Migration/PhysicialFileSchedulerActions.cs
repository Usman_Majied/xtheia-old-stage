﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysicialFileSchedulerActions
    {
        public PhysicialFileSchedulerActions()
        {
            InversePhysicialFileSchedulerActionsNavigation = new HashSet<PhysicialFileSchedulerActions>();
        }

        public int PhysicialFileId { get; set; }
        public int SchedulerActionId { get; set; }
        public int ProcessingStatusId { get; set; }
        public int? MergeLocation { get; set; }
        public int? PreviousMergeLocation { get; set; }
        public string DestFileName { get; set; }
        public string RelativeSrcPath { get; set; }
        public string RelativeDestPath { get; set; }
        public string Error { get; set; }
        public int Id { get; set; }
        public int? PhysicialFileSchedulerActionsId { get; set; }
        public string SrcFileName { get; set; }
        public string ExtraInfo { get; set; }

        public virtual PhysicialFiles PhysicialFile { get; set; }
        public virtual PhysicialFileSchedulerActions PhysicialFileSchedulerActionsNavigation { get; set; }
        public virtual ProcessingStatuses ProcessingStatus { get; set; }
        public virtual SchedulerActions SchedulerAction { get; set; }
        public virtual ICollection<PhysicialFileSchedulerActions> InversePhysicialFileSchedulerActionsNavigation { get; set; }
    }
}
