﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class ConsentForm
    {
        public ConsentForm()
        {
            PatientConsent = new HashSet<PatientConsent>();
        }

        public int Id { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Code { get; set; }
        public string Url { get; set; }

        public virtual ICollection<PatientConsent> PatientConsent { get; set; }
    }
}
