﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class UsersSecurityQuestions
    {
        public int SecurityQuestionId { get; set; }
        public long UserId { get; set; }
        public string Answer { get; set; }

        public virtual SecurityQuestions SecurityQuestion { get; set; }
        public virtual AspNetUsers User { get; set; }
    }
}
