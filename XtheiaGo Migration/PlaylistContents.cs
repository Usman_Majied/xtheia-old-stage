﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PlaylistContents
    {
        public int PlaylistContentsId { get; set; }
        public int PlaylistId { get; set; }
        public int ContentId { get; set; }
        public int SequenceNo { get; set; }
        public int? RepeatedCount { get; set; }

        public virtual Contents Content { get; set; }
        public virtual Playlists Playlist { get; set; }
    }
}
