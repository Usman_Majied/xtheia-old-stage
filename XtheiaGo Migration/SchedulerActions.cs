﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class SchedulerActions
    {
        public SchedulerActions()
        {
            PhysicialFileSchedulerActions = new HashSet<PhysicialFileSchedulerActions>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PhysicialFileSchedulerActions> PhysicialFileSchedulerActions { get; set; }
    }
}
