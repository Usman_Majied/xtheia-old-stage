﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace XtheiaGo_Migration
{
    public partial class xtheiaoldstageContext : DbContext
    {
        public xtheiaoldstageContext()
        {
        }

        public xtheiaoldstageContext(DbContextOptions<xtheiaoldstageContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AccountTypes> AccountTypes { get; set; }
        public virtual DbSet<Accounts> Accounts { get; set; }
        public virtual DbSet<AspNetRoleClaims> AspNetRoleClaims { get; set; }
        public virtual DbSet<AspNetRoles> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public virtual DbSet<AspNetUserTokens> AspNetUserTokens { get; set; }
        public virtual DbSet<AspNetUsers> AspNetUsers { get; set; }
        public virtual DbSet<CommericalAssigments> CommericalAssigments { get; set; }
        public virtual DbSet<Commericals> Commericals { get; set; }
        public virtual DbSet<ConsentForm> ConsentForm { get; set; }
        public virtual DbSet<ContentCatagories> ContentCatagories { get; set; }
        public virtual DbSet<ContentTypes> ContentTypes { get; set; }
        public virtual DbSet<Contents> Contents { get; set; }
        public virtual DbSet<Countries> Countries { get; set; }
        public virtual DbSet<DeployedPlaylists> DeployedPlaylists { get; set; }
        public virtual DbSet<DeviceTypes> DeviceTypes { get; set; }
        public virtual DbSet<Devices> Devices { get; set; }
        public virtual DbSet<PartCategories> PartCategories { get; set; }
        public virtual DbSet<PartLocations> PartLocations { get; set; }
        public virtual DbSet<PartTypes> PartTypes { get; set; }
        public virtual DbSet<Parts> Parts { get; set; }
        public virtual DbSet<PatientConsent> PatientConsent { get; set; }
        public virtual DbSet<PatientContentTypes> PatientContentTypes { get; set; }
        public virtual DbSet<Patients> Patients { get; set; }
        public virtual DbSet<PatientsFaceRecognition> PatientsFaceRecognition { get; set; }
        public virtual DbSet<PhysicialFileSchedulerActions> PhysicialFileSchedulerActions { get; set; }
        public virtual DbSet<PhysicialFiles> PhysicialFiles { get; set; }
        public virtual DbSet<PhysicialSubFiles> PhysicialSubFiles { get; set; }
        public virtual DbSet<PhysicianContents> PhysicianContents { get; set; }
        public virtual DbSet<PhysicianPatients> PhysicianPatients { get; set; }
        public virtual DbSet<PhysiciansPatientsContents> PhysiciansPatientsContents { get; set; }
        public virtual DbSet<PhysiciansPatientsContentsVersion> PhysiciansPatientsContentsVersion { get; set; }
        public virtual DbSet<PlaylistContents> PlaylistContents { get; set; }
        public virtual DbSet<PlaylistTypes> PlaylistTypes { get; set; }
        public virtual DbSet<Playlists> Playlists { get; set; }
        public virtual DbSet<ProcessingStatuses> ProcessingStatuses { get; set; }
        public virtual DbSet<RelatedFiles> RelatedFiles { get; set; }
        public virtual DbSet<SchedulerActions> SchedulerActions { get; set; }
        public virtual DbSet<SecurityQuestions> SecurityQuestions { get; set; }
        public virtual DbSet<States> States { get; set; }
        public virtual DbSet<StatusTypes> StatusTypes { get; set; }
        public virtual DbSet<Titles> Titles { get; set; }
        public virtual DbSet<UsersSecurityQuestions> UsersSecurityQuestions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseMySql("server=localhost;port=3307;database=xtheia-old-stage;user=root;password=LAaZaUuh4rMLHp3Q;treattinyasboolean=true", x => x.ServerVersion("8.0.22-mysql"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccountTypes>(entity =>
            {
                entity.ToTable("account_types");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Accounts>(entity =>
            {
                entity.HasKey(e => e.AccountId)
                    .HasName("PRIMARY");

                entity.ToTable("accounts");

                entity.HasIndex(e => e.AccountTypeId)
                    .HasName("accounts_accounts_types_id_fk_idx");

                entity.HasIndex(e => e.CountryId)
                    .HasName("accounts_country_id_fk_idx");

                entity.HasIndex(e => e.StateId)
                    .HasName("accounts_states_id_fk_idx");

                entity.HasIndex(e => e.StatusId)
                    .HasName("accounts_status_id_fk_idx");

                entity.HasIndex(e => e.TitleId)
                    .HasName("accounts_titles_id_fk_idx");

                entity.HasIndex(e => e.TitleIdSecondary)
                    .HasName("accounts_titles_secondary_id_fk_idx");

                entity.Property(e => e.AccountId).HasColumnName("account_id");

                entity.Property(e => e.AccountTypeId).HasColumnName("account_type_id");

                entity.Property(e => e.BusinessName)
                    .IsRequired()
                    .HasColumnName("business_name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.City)
                    .IsRequired()
                    .HasColumnName("city")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Country)
                    .HasColumnName("country")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EmailPrimary)
                    .HasColumnName("email_primary")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.EmailSecondary)
                    .HasColumnName("email_secondary")
                    .HasColumnType("varchar(45)")
                    .HasComment("2’s are secondary details")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.EmployeeNumber)
                    .HasColumnType("varchar(6)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.IsPreimum)
                    .HasColumnName("is_preimum")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.NamePrimary)
                    .HasColumnName("name_primary")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.NameSecondary)
                    .HasColumnName("name_secondary")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Phone)
                    .IsRequired()
                    .HasColumnName("phone")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PhonePrimary)
                    .HasColumnName("phone_primary")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PhoneSecondary)
                    .HasColumnName("phone_secondary")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.StateId).HasColumnName("state_id");

                entity.Property(e => e.StatusId).HasColumnName("status_Id");

                entity.Property(e => e.Street)
                    .IsRequired()
                    .HasColumnName("street")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Suit)
                    .HasColumnName("suit")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.TitleIdSecondary).HasColumnName("TitleId_secondary");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.ZipCode)
                    .IsRequired()
                    .HasColumnName("zip_code")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.AccountType)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.AccountTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("accounts_accounts_types_id_fk");

                entity.HasOne(d => d.CountryNavigation)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.CountryId)
                    .HasConstraintName("accounts_country_id_fk");

                entity.HasOne(d => d.State)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.StateId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("accounts_states_id_fk");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Accounts)
                    .HasForeignKey(d => d.StatusId)
                    .HasConstraintName("accounts_status_id_fk");

                entity.HasOne(d => d.Title)
                    .WithMany(p => p.AccountsTitle)
                    .HasForeignKey(d => d.TitleId)
                    .HasConstraintName("accounts_titles_id_fk");

                entity.HasOne(d => d.TitleIdSecondaryNavigation)
                    .WithMany(p => p.AccountsTitleIdSecondaryNavigation)
                    .HasForeignKey(d => d.TitleIdSecondary)
                    .HasConstraintName("accounts_titles_secondary_id_fk");
            });

            modelBuilder.Entity<AspNetRoleClaims>(entity =>
            {
                entity.HasIndex(e => e.RoleId);

                entity.Property(e => e.ClaimType)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ClaimValue)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetRoleClaims)
                    .HasForeignKey(d => d.RoleId);
            });

            modelBuilder.Entity<AspNetRoles>(entity =>
            {
                entity.HasIndex(e => e.NormalizedName)
                    .HasName("RoleNameIndex")
                    .IsUnique();

                entity.Property(e => e.ConcurrencyStamp)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Name)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.NormalizedName)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<AspNetUserClaims>(entity =>
            {
                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.ClaimType)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ClaimValue)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserClaims)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserLogins>(entity =>
            {
                entity.HasKey(e => new { e.LoginProvider, e.ProviderKey })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.HasIndex(e => e.UserId);

                entity.Property(e => e.LoginProvider)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ProviderKey)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ProviderDisplayName)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserLogins)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserRoles>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.RoleId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.HasIndex(e => e.RoleId);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull);

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserRoles)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUserTokens>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.LoginProvider, e.Name })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0, 0 });

                entity.Property(e => e.LoginProvider)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Name)
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Value)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.AspNetUserTokens)
                    .HasForeignKey(d => d.UserId);
            });

            modelBuilder.Entity<AspNetUsers>(entity =>
            {
                entity.HasIndex(e => e.AccountId)
                    .HasName("AspnetUser_Account_AccountId_Fk_idx");

                entity.HasIndex(e => e.NormalizedEmail)
                    .HasName("EmailIndex");

                entity.HasIndex(e => e.NormalizedUserName)
                    .HasName("UserNameIndex")
                    .IsUnique();

                entity.HasIndex(e => e.TitleId)
                    .HasName("Aspnetuser_Title_TitleId_FK_idx");

                entity.Property(e => e.AvatarPath)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ConcurrencyStamp)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.ConnectState)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CreateAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.DropboxAccessToken)
                    .HasColumnType("varchar(2000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Email)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.GoogleAccessToken)
                    .HasColumnType("varchar(2000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Name)
                    .HasColumnType("varchar(128)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.NormalizedEmail)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.NormalizedUserName)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.OneDriveToken)
                    .HasColumnType("varchar(2000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PasswordHash)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PhoneNumber)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.SecurityStamp)
                    .HasColumnType("longtext")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.UpdateAt)
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.Property(e => e.UserName)
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.AspNetUsers)
                    .HasForeignKey(d => d.AccountId)
                    .HasConstraintName("AspnetUser_Account_AccountId_Fk");

                entity.HasOne(d => d.Title)
                    .WithMany(p => p.AspNetUsers)
                    .HasForeignKey(d => d.TitleId)
                    .HasConstraintName("Aspnetuser_Title_TitleId_FK");
            });

            modelBuilder.Entity<CommericalAssigments>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("commerical_assigments");

                entity.HasIndex(e => e.CommericalId)
                    .HasName("commericals_commerical_id_idx");

                entity.HasIndex(e => e.CustomerId)
                    .HasName("customers_aspnetuser_id_pk_idx");

                entity.Property(e => e.CommericalId).HasColumnName("commerical_id");

                entity.Property(e => e.CustomerId).HasColumnName("customer_id");

                entity.Property(e => e.IsInactive)
                    .HasColumnName("is_inactive")
                    .HasDefaultValueSql("'1'");

                entity.HasOne(d => d.Commerical)
                    .WithMany()
                    .HasForeignKey(d => d.CommericalId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("commericals_commerical_id");

                entity.HasOne(d => d.Customer)
                    .WithMany()
                    .HasForeignKey(d => d.CustomerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("customers_aspnetuser_id_pk");
            });

            modelBuilder.Entity<Commericals>(entity =>
            {
                entity.HasKey(e => e.CommericalId)
                    .HasName("PRIMARY");

                entity.ToTable("commericals");

                entity.Property(e => e.CommericalId).HasColumnName("commerical_id");

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("datetime");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.RevisionNumber)
                    .HasColumnName("revision_number")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.VenderName)
                    .HasColumnName("vender_name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.VenderNumber).HasColumnName("vender_number");
            });

            modelBuilder.Entity<ConsentForm>(entity =>
            {
                entity.ToTable("consent_form");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasColumnName("code")
                    .HasColumnType("mediumtext")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasColumnName("description")
                    .HasColumnType("varchar(128)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasColumnName("title")
                    .HasColumnType("varchar(128)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Url)
                    .HasColumnName("url")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");
            });

            modelBuilder.Entity<ContentCatagories>(entity =>
            {
                entity.ToTable("content_catagories");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<ContentTypes>(entity =>
            {
                entity.ToTable("content_types");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Contents>(entity =>
            {
                entity.HasKey(e => e.ContentId)
                    .HasName("PRIMARY");

                entity.ToTable("contents");

                entity.HasIndex(e => e.CatagoryId)
                    .HasName("contents_content_catagories_id_fk_idx");

                entity.HasIndex(e => e.OwnerId)
                    .HasName("Contents_AspNetUsers_idx");

                entity.HasIndex(e => e.PhysicialFileId)
                    .HasName("contents_content_types_id_fk_idx");

                entity.HasIndex(e => e.StatusId)
                    .HasName("contents_status_types_id_fk_idx");

                entity.Property(e => e.ContentId).HasColumnName("content_id");

                entity.Property(e => e.BackgroundRemoval)
                    .HasColumnName("background_removal")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.CatagoryId).HasColumnName("catagory_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(200)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.OwnerId).HasColumnName("owner_id");

                entity.Property(e => e.PhysicialFileId).HasColumnName("physicial_file_id");

                entity.Property(e => e.PreimumStatus)
                    .HasColumnName("preimum_status")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ProductCatagory)
                    .HasColumnName("product_catagory")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.RevisionNumber)
                    .HasColumnName("revision_number")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.SharedGlobally)
                    .HasColumnName("shared_globally")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.Property(e => e.TypeId).HasColumnName("type_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.Catagory)
                    .WithMany(p => p.Contents)
                    .HasForeignKey(d => d.CatagoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("contents_content_catagories_id_fk");

                entity.HasOne(d => d.Owner)
                    .WithMany(p => p.Contents)
                    .HasForeignKey(d => d.OwnerId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("Contents_AspNetUsers");

                entity.HasOne(d => d.PhysicialFile)
                    .WithMany(p => p.Contents)
                    .HasForeignKey(d => d.PhysicialFileId)
                    .HasConstraintName("contents_content_types_id_fk");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Contents)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("contents_status_types_id_fk");
            });

            modelBuilder.Entity<Countries>(entity =>
            {
                entity.ToTable("countries");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CountryCode)
                    .IsRequired()
                    .HasColumnName("country_code")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CountryName)
                    .IsRequired()
                    .HasColumnName("country_name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<DeployedPlaylists>(entity =>
            {
                entity.HasKey(e => e.DeployedPlaylistId)
                    .HasName("PRIMARY");

                entity.ToTable("deployed_playlists");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("deployed_playlists_device_id_fk_idx");

                entity.HasIndex(e => e.PlaylistId)
                    .HasName("deployed_playlists_playlist_id_fk");

                entity.Property(e => e.DeployedPlaylistId).HasColumnName("deployed_playlist_id");

                entity.Property(e => e.DeviceId).HasColumnName("device_id");

                entity.Property(e => e.IsDeployed).HasColumnName("is_deployed");

                entity.Property(e => e.PlaylistId).HasColumnName("playlist_id");

                entity.Property(e => e.ReleaseDate)
                    .HasColumnName("release_date")
                    .HasColumnType("date");

                entity.HasOne(d => d.Playlist)
                    .WithMany(p => p.DeployedPlaylists)
                    .HasForeignKey(d => d.PlaylistId)
                    .HasConstraintName("deployed_playlists_playlist_id_fk");
            });

            modelBuilder.Entity<DeviceTypes>(entity =>
            {
                entity.ToTable("device_types");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Devices>(entity =>
            {
                entity.HasKey(e => new { e.DeviceId, e.DeviceTypeId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("devices");

                entity.HasIndex(e => e.AssignedUserId)
                    .HasName("devices_users_id_fk_idx");

                entity.HasIndex(e => e.DeviceTypeId)
                    .HasName("devices_device_types_id_fk_idx");

                entity.HasIndex(e => e.StatusId)
                    .HasName("devices_status_id_fk_idx");

                entity.Property(e => e.DeviceId)
                    .HasColumnName("device_id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.DeviceTypeId).HasColumnName("device_type_id");

                entity.Property(e => e.AssignedUserId).HasColumnName("assigned_user_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Sku)
                    .HasColumnName("sku")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.StatusId).HasColumnName("status_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.AssignedUser)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.AssignedUserId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("devices_aspnetsuer_id_fk");

                entity.HasOne(d => d.DeviceType)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.DeviceTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("devices_device_types_id_fk");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Devices)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("devices_status_id_fk");
            });

            modelBuilder.Entity<PartCategories>(entity =>
            {
                entity.ToTable("part_categories");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<PartLocations>(entity =>
            {
                entity.ToTable("part_locations");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.LocationName)
                    .IsRequired()
                    .HasColumnName("location_name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<PartTypes>(entity =>
            {
                entity.ToTable("part_types");

                entity.HasIndex(e => e.PartCategoryId)
                    .HasName("part_types_part_catagories_id_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PartCategoryId).HasColumnName("part_category_id");

                entity.HasOne(d => d.PartCategory)
                    .WithMany(p => p.PartTypes)
                    .HasForeignKey(d => d.PartCategoryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("part_types_part_catagories_id");
            });

            modelBuilder.Entity<Parts>(entity =>
            {
                entity.HasKey(e => e.PartId)
                    .HasName("PRIMARY");

                entity.ToTable("parts");

                entity.HasIndex(e => e.DeviceId)
                    .HasName("device_parts_id_fk_idx");

                entity.HasIndex(e => e.PartLocationId)
                    .HasName("part_part_location_fk_idx");

                entity.HasIndex(e => e.PartTypeId)
                    .HasName("part_types_id_idx");

                entity.HasIndex(e => new { e.PartTypeId, e.SerialNo })
                    .HasName("parts_parts_type_id_serial_no_unique")
                    .IsUnique();

                entity.Property(e => e.PartId).HasColumnName("part_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.DeviceId).HasColumnName("device_id");

                entity.Property(e => e.FirmwareDate)
                    .HasColumnName("firmware_date")
                    .HasColumnType("date");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PartLocationId).HasColumnName("part_location_id");

                entity.Property(e => e.PartTypeId).HasColumnName("part_type_id");

                entity.Property(e => e.SerialNo)
                    .IsRequired()
                    .HasColumnName("serial_no")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.PartLocation)
                    .WithMany(p => p.Parts)
                    .HasForeignKey(d => d.PartLocationId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("part_part_location_fk");

                entity.HasOne(d => d.PartType)
                    .WithMany(p => p.Parts)
                    .HasForeignKey(d => d.PartTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("part_types_id_fk");
            });

            modelBuilder.Entity<PatientConsent>(entity =>
            {
                entity.ToTable("patient_consent");

                entity.HasIndex(e => e.ConsentId)
                    .HasName("patient_consent_consent_id_fk_idx");

                entity.HasIndex(e => e.PhysicianPatientsId)
                    .HasName("patient_conset_physician_patients_id_fk_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ConsentId).HasColumnName("consent_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EncodedPath)
                    .IsRequired()
                    .HasColumnName("encoded_path")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Ext)
                    .IsRequired()
                    .HasColumnName("ext")
                    .HasColumnType("varchar(6)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnName("filename")
                    .HasColumnType("varchar(128)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PhysicianPatientsId).HasColumnName("physician_patients_id");

                entity.Property(e => e.ThumbnailEncodedPath)
                    .IsRequired()
                    .HasColumnName("thumbnail_encoded_path")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.Consent)
                    .WithMany(p => p.PatientConsent)
                    .HasForeignKey(d => d.ConsentId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("patient_consent_consent_id_fk");

                entity.HasOne(d => d.PhysicianPatients)
                    .WithMany(p => p.PatientConsent)
                    .HasForeignKey(d => d.PhysicianPatientsId)
                    .HasConstraintName("patient_conset_physician_patients_id_fk");
            });

            modelBuilder.Entity<PatientContentTypes>(entity =>
            {
                entity.ToTable("patient_content_types");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Patients>(entity =>
            {
                entity.HasKey(e => e.PatientId)
                    .HasName("PRIMARY");

                entity.ToTable("patients");

                entity.HasIndex(e => e.Email)
                    .HasName("email_UNIQUE")
                    .IsUnique();

                entity.HasIndex(e => new { e.Name, e.Dob, e.Ssn })
                    .HasName("name_dob_ssn__UNIQUE KEY")
                    .IsUnique();

                entity.Property(e => e.PatientId)
                    .HasColumnName("patient_id")
                    .HasComment("It is not auto increment, it will be entered from front end");

                entity.Property(e => e.Dob)
                    .HasColumnName("dob")
                    .HasColumnType("date");

                entity.Property(e => e.Email)
                    .IsRequired()
                    .HasColumnName("email")
                    .HasColumnType("varchar(256)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_general_ci");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PatientNo)
                    .IsRequired()
                    .HasColumnName("patient_no")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Ssn)
                    .IsRequired()
                    .HasColumnName("ssn")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<PatientsFaceRecognition>(entity =>
            {
                entity.ToTable("patients_face_recognition");

                entity.HasIndex(e => e.PatientId)
                    .HasName("patients_face_recognition_patients_id_fk_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.ImageBinding)
                    .IsRequired()
                    .HasColumnName("image_binding")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.OrginalImage)
                    .IsRequired()
                    .HasColumnName("orginal_image")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.OrginalImagePath)
                    .IsRequired()
                    .HasColumnName("orginal_image_path")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.PatientId).HasColumnName("patient_id");

                entity.Property(e => e.SourceFolder)
                    .IsRequired()
                    .HasColumnName("source_folder")
                    .HasColumnType("varchar(10000)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.Patient)
                    .WithMany(p => p.PatientsFaceRecognition)
                    .HasForeignKey(d => d.PatientId)
                    .HasConstraintName("patients_face_recognition_patients_id_fk");
            });

            modelBuilder.Entity<PhysicialFileSchedulerActions>(entity =>
            {
                entity.ToTable("physicial_file_scheduler_actions");

                entity.HasIndex(e => e.PhysicialFileId)
                    .HasName("physicial_file_id_fk");

                entity.HasIndex(e => e.PhysicialFileSchedulerActionsId)
                    .HasName("physicial_file_scheduler_actions_id_self_fk_idx");

                entity.HasIndex(e => e.ProcessingStatusId)
                    .HasName("scheduler_processing_status_id_fk_idx");

                entity.HasIndex(e => e.SchedulerActionId)
                    .HasName("scheduler_id_fk_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.DestFileName)
                    .IsRequired()
                    .HasColumnName("dest_file_name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.Error)
                    .HasColumnName("error")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.ExtraInfo)
                    .HasColumnName("extra_info")
                    .HasColumnType("json");

                entity.Property(e => e.MergeLocation)
                    .HasColumnName("merge_location")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.PhysicialFileId).HasColumnName("physicial_file_id");

                entity.Property(e => e.PhysicialFileSchedulerActionsId).HasColumnName("physicial_file_scheduler_actions_id");

                entity.Property(e => e.PreviousMergeLocation)
                    .HasColumnName("previous_merge_location")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.ProcessingStatusId)
                    .HasColumnName("processing_status_id")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.RelativeDestPath)
                    .IsRequired()
                    .HasColumnName("relative_dest_path")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.RelativeSrcPath)
                    .IsRequired()
                    .HasColumnName("relative_src_path")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_520_ci");

                entity.Property(e => e.SchedulerActionId).HasColumnName("scheduler_action_id");

                entity.Property(e => e.SrcFileName)
                    .HasColumnName("src_file_name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_520_ci");

                entity.HasOne(d => d.PhysicialFile)
                    .WithMany(p => p.PhysicialFileSchedulerActions)
                    .HasForeignKey(d => d.PhysicialFileId)
                    .HasConstraintName("physicial_file_id_fk");

                entity.HasOne(d => d.PhysicialFileSchedulerActionsNavigation)
                    .WithMany(p => p.InversePhysicialFileSchedulerActionsNavigation)
                    .HasForeignKey(d => d.PhysicialFileSchedulerActionsId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("physicial_file_scheduler_actions_id_self_fk");

                entity.HasOne(d => d.ProcessingStatus)
                    .WithMany(p => p.PhysicialFileSchedulerActions)
                    .HasForeignKey(d => d.ProcessingStatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("scheduler_processing_status_id_fk");

                entity.HasOne(d => d.SchedulerAction)
                    .WithMany(p => p.PhysicialFileSchedulerActions)
                    .HasForeignKey(d => d.SchedulerActionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("scheduler_id_fk");
            });

            modelBuilder.Entity<PhysicialFiles>(entity =>
            {
                entity.ToTable("physicial_files");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EncodedPath)
                    .IsRequired()
                    .HasColumnName("encoded_path")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Ext)
                    .IsRequired()
                    .HasColumnName("ext")
                    .HasColumnType("varchar(6)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.ExtraInfo)
                    .HasColumnName("extra_info")
                    .HasColumnType("json");

                entity.Property(e => e.Filename)
                    .IsRequired()
                    .HasColumnName("filename")
                    .HasColumnType("varchar(128)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasColumnType("time");

                entity.Property(e => e.PreviousMergeLocation)
                    .HasColumnName("previous_merge_location")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();
            });

            modelBuilder.Entity<PhysicialSubFiles>(entity =>
            {
                entity.HasKey(e => new { e.PhysicialFileId, e.RelatedFileId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("physicial_sub_files");

                entity.HasIndex(e => e.PhysicialFileId)
                    .HasName("physicial_sub_files_physicial_id_fk_idx");

                entity.HasIndex(e => e.RelatedFileId)
                    .HasName("physicial_sub_files_resolutions_id_fk_idx");

                entity.Property(e => e.PhysicialFileId).HasColumnName("physicial_file_id");

                entity.Property(e => e.RelatedFileId).HasColumnName("related_file_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.EncodedPath)
                    .IsRequired()
                    .HasColumnName("encoded_path")
                    .HasColumnType("varchar(300)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.PhysicialFile)
                    .WithMany(p => p.PhysicialSubFiles)
                    .HasForeignKey(d => d.PhysicialFileId)
                    .HasConstraintName("physicial_sub_files_physicial_id_fk");
            });

            modelBuilder.Entity<PhysicianContents>(entity =>
            {
                entity.HasKey(e => new { e.ContentId, e.PhysicianId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("physician_contents");

                entity.HasIndex(e => e.PhysicianId)
                    .HasName("physician_contents_physician_id_fk_idx");

                entity.Property(e => e.ContentId).HasColumnName("content_id");

                entity.Property(e => e.PhysicianId).HasColumnName("physician_id");

                entity.HasOne(d => d.Content)
                    .WithMany(p => p.PhysicianContents)
                    .HasForeignKey(d => d.ContentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("physician_contents_contents_id_fk");
            });

            modelBuilder.Entity<PhysicianPatients>(entity =>
            {
                entity.ToTable("physician_patients");

                entity.HasIndex(e => e.PatientId)
                    .HasName("physician_patients_patient_id_fk_idx");

                entity.HasIndex(e => e.PhysicianId)
                    .HasName("physician_patients_physician_id_fk_idx");

                entity.Property(e => e.PhysicianPatientsId).HasColumnName("physician_patients_id");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.PatientId).HasColumnName("patient_id");

                entity.Property(e => e.PhysicianId).HasColumnName("physician_id");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.Patient)
                    .WithMany(p => p.PhysicianPatients)
                    .HasForeignKey(d => d.PatientId)
                    .HasConstraintName("physician_patients_patient_id_fk");

                entity.HasOne(d => d.Physician)
                    .WithMany(p => p.PhysicianPatients)
                    .HasForeignKey(d => d.PhysicianId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("physician_AspnetUsers_id_fk");
            });

            modelBuilder.Entity<PhysiciansPatientsContents>(entity =>
            {
                entity.ToTable("physicians_patients_contents");

                entity.HasIndex(e => e.PatientContentTypeId)
                    .HasName("physicians_patients_contents_patient_content_types_id_fk_idx");

                entity.HasIndex(e => e.PhysicialFileId)
                    .HasName("physicians_patients_contents_physicial_file_id_fk_idx");

                entity.HasIndex(e => e.PhysicianPatientId)
                    .HasName("physicians_patients_contents_physician_patients_id_fk_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BodyParts)
                    .HasColumnName("body_parts")
                    .HasColumnType("varchar(128)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PatientContentTypeId).HasColumnName("patient_content_type_id");

                entity.Property(e => e.PhysicialFileId).HasColumnName("physicial_file_id");

                entity.Property(e => e.PhysicianPatientId).HasColumnName("physician_patient_id");

                entity.Property(e => e.SharedGlobally)
                    .HasColumnName("shared_globally")
                    .HasDefaultValueSql("'0'");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.PatientContentType)
                    .WithMany(p => p.PhysiciansPatientsContents)
                    .HasForeignKey(d => d.PatientContentTypeId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("physicians_patients_contents_patient_content_types_id_fk");

                entity.HasOne(d => d.PhysicialFile)
                    .WithMany(p => p.PhysiciansPatientsContents)
                    .HasForeignKey(d => d.PhysicialFileId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("physicians_patients_contents_physicial_file_id_fk");

                entity.HasOne(d => d.PhysicianPatient)
                    .WithMany(p => p.PhysiciansPatientsContents)
                    .HasForeignKey(d => d.PhysicianPatientId)
                    .HasConstraintName("physicians_patients_contents_physician_patients_id_fk");
            });

            modelBuilder.Entity<PhysiciansPatientsContentsVersion>(entity =>
            {
                entity.ToTable("physicians_patients_contents_version");

                entity.HasIndex(e => e.PhysicialFileId)
                    .HasName("physicians_patients_contents_version_physicial_file_id_fk_idx");

                entity.HasIndex(e => e.PhysicianPatientsContentsId)
                    .HasName("physician_patients-contents_version_physician_patients-cont_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.ContentPath)
                    .HasColumnName("content_path")
                    .HasColumnType("varchar(255)")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Notes)
                    .HasColumnName("notes")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.PhysicialFileId).HasColumnName("physicial_file_id");

                entity.Property(e => e.PhysicianPatientsContentsId).HasColumnName("physician_patients_contents_id");

                entity.Property(e => e.TextInImage)
                    .HasColumnName("text_in_image")
                    .HasColumnType("text")
                    .HasCharSet("utf8")
                    .HasCollation("utf8_unicode_ci");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.PhysicialFile)
                    .WithMany(p => p.PhysiciansPatientsContentsVersion)
                    .HasForeignKey(d => d.PhysicialFileId)
                    .HasConstraintName("physicians_patients_contents_version_physicial_file_id_fk");

                entity.HasOne(d => d.PhysicianPatientsContents)
                    .WithMany(p => p.PhysiciansPatientsContentsVersion)
                    .HasForeignKey(d => d.PhysicianPatientsContentsId)
                    .OnDelete(DeleteBehavior.Cascade)
                    .HasConstraintName("physician_version_physician_patients-contents_id_fk");
            });

            modelBuilder.Entity<PlaylistContents>(entity =>
            {
                entity.ToTable("playlist_contents");

                entity.HasIndex(e => e.ContentId)
                    .HasName("playlists_contents_contents_id_fk_idx");

                entity.HasIndex(e => e.PlaylistId)
                    .HasName("playlists_contents_playlist_id_fk");

                entity.Property(e => e.PlaylistContentsId).HasColumnName("playlist_contents_id");

                entity.Property(e => e.ContentId).HasColumnName("content_id");

                entity.Property(e => e.PlaylistId).HasColumnName("playlist_id");

                entity.Property(e => e.RepeatedCount)
                    .HasColumnName("repeated_count")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.SequenceNo).HasColumnName("sequence_no");

                entity.HasOne(d => d.Content)
                    .WithMany(p => p.PlaylistContents)
                    .HasForeignKey(d => d.ContentId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("playlists_contents_contents_id_fk");

                entity.HasOne(d => d.Playlist)
                    .WithMany(p => p.PlaylistContents)
                    .HasForeignKey(d => d.PlaylistId)
                    .HasConstraintName("playlists_contents_playlist_id_fk");
            });

            modelBuilder.Entity<PlaylistTypes>(entity =>
            {
                entity.ToTable("playlist_types");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Playlists>(entity =>
            {
                entity.HasKey(e => e.PlaylistId)
                    .HasName("PRIMARY");

                entity.ToTable("playlists");

                entity.HasIndex(e => e.AssignedUserId)
                    .HasName("playlists_users_id_fk_idx");

                entity.HasIndex(e => e.PlaylistTypeId)
                    .HasName("playlists_ playlists_types_id_fk_idx");

                entity.Property(e => e.PlaylistId).HasColumnName("playlist_id");

                entity.Property(e => e.AssignedUserId)
                    .HasColumnName("assigned_user_id")
                    .HasComment("Allowing this as nullable, since when creating playlist, and will be assigned latter");

                entity.Property(e => e.CreatedAt)
                    .HasColumnName("created_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP");

                entity.Property(e => e.Description)
                    .HasColumnName("description")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.Length)
                    .HasColumnName("length")
                    .HasColumnType("time");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(100)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.PlaylistTypeId).HasColumnName("playlist_type_id");

                entity.Property(e => e.RepeatCount)
                    .HasColumnName("repeat_count")
                    .HasDefaultValueSql("'-1'");

                entity.Property(e => e.UpdatedAt)
                    .HasColumnName("updated_at")
                    .HasColumnType("timestamp")
                    .HasDefaultValueSql("CURRENT_TIMESTAMP")
                    .ValueGeneratedOnAddOrUpdate();

                entity.HasOne(d => d.AssignedUser)
                    .WithMany(p => p.Playlists)
                    .HasForeignKey(d => d.AssignedUserId)
                    .OnDelete(DeleteBehavior.SetNull)
                    .HasConstraintName("playlists_ AspnetUser_Assign_user_id_fk");

                entity.HasOne(d => d.PlaylistType)
                    .WithMany(p => p.Playlists)
                    .HasForeignKey(d => d.PlaylistTypeId)
                    .HasConstraintName("playlists_ playlists_types_id_fk");
            });

            modelBuilder.Entity<ProcessingStatuses>(entity =>
            {
                entity.ToTable("processing_statuses");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<RelatedFiles>(entity =>
            {
                entity.HasKey(e => new { e.Id, e.Name })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("related_files");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedOnAdd();

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<SchedulerActions>(entity =>
            {
                entity.ToTable("scheduler_actions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_unicode_520_ci");
            });

            modelBuilder.Entity<SecurityQuestions>(entity =>
            {
                entity.ToTable("security_questions");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Question)
                    .HasColumnName("question")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<States>(entity =>
            {
                entity.ToTable("states");

                entity.HasIndex(e => e.CountryId)
                    .HasName("states_countries_id_fk_idx");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Abbr)
                    .IsRequired()
                    .HasColumnName("abbr")
                    .HasColumnType("varchar(2)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.Property(e => e.CountryId).HasColumnName("country_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(250)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.Country)
                    .WithMany(p => p.States)
                    .HasForeignKey(d => d.CountryId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("states_countries_id_fk");
            });

            modelBuilder.Entity<StatusTypes>(entity =>
            {
                entity.ToTable("status_types");

                entity.Property(e => e.Id)
                    .HasColumnName("id")
                    .ValueGeneratedNever();

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<Titles>(entity =>
            {
                entity.ToTable("titles");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Name)
                    .HasColumnName("name")
                    .HasColumnType("varchar(45)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");
            });

            modelBuilder.Entity<UsersSecurityQuestions>(entity =>
            {
                entity.HasKey(e => new { e.SecurityQuestionId, e.UserId })
                    .HasName("PRIMARY")
                    .HasAnnotation("MySql:IndexPrefixLength", new[] { 0, 0 });

                entity.ToTable("users_security_questions");

                entity.HasIndex(e => e.UserId)
                    .HasName("users_id_fk_idx");

                entity.Property(e => e.SecurityQuestionId).HasColumnName("security_question_id");

                entity.Property(e => e.UserId).HasColumnName("user_id");

                entity.Property(e => e.Answer)
                    .HasColumnName("answer")
                    .HasColumnType("varchar(1000)")
                    .HasCharSet("utf8mb4")
                    .HasCollation("utf8mb4_0900_ai_ci");

                entity.HasOne(d => d.SecurityQuestion)
                    .WithMany(p => p.UsersSecurityQuestions)
                    .HasForeignKey(d => d.SecurityQuestionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("users_security_questions_security_questions_id_fk");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UsersSecurityQuestions)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("users_security_questions_aspnetusers_fk");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
