﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PlaylistTypes
    {
        public PlaylistTypes()
        {
            Playlists = new HashSet<Playlists>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Playlists> Playlists { get; set; }
    }
}
