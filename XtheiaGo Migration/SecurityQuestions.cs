﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class SecurityQuestions
    {
        public SecurityQuestions()
        {
            UsersSecurityQuestions = new HashSet<UsersSecurityQuestions>();
        }

        public int Id { get; set; }
        public string Question { get; set; }

        public virtual ICollection<UsersSecurityQuestions> UsersSecurityQuestions { get; set; }
    }
}
