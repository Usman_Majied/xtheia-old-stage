﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class CommericalAssigments
    {
        public long CustomerId { get; set; }
        public int CommericalId { get; set; }
        public sbyte? IsInactive { get; set; }

        public virtual Commericals Commerical { get; set; }
        public virtual AspNetUsers Customer { get; set; }
    }
}
