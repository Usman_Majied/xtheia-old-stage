﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysicianPatients
    {
        public PhysicianPatients()
        {
            PatientConsent = new HashSet<PatientConsent>();
            PhysiciansPatientsContents = new HashSet<PhysiciansPatientsContents>();
        }

        public int PhysicianPatientsId { get; set; }
        public long PhysicianId { get; set; }
        public int PatientId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual Patients Patient { get; set; }
        public virtual AspNetUsers Physician { get; set; }
        public virtual ICollection<PatientConsent> PatientConsent { get; set; }
        public virtual ICollection<PhysiciansPatientsContents> PhysiciansPatientsContents { get; set; }
    }
}
