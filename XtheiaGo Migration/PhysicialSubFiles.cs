﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysicialSubFiles
    {
        public int PhysicialFileId { get; set; }
        public int RelatedFileId { get; set; }
        public string EncodedPath { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }

        public virtual PhysicialFiles PhysicialFile { get; set; }
    }
}
