﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PartLocations
    {
        public PartLocations()
        {
            Parts = new HashSet<Parts>();
        }

        public int Id { get; set; }
        public string LocationName { get; set; }

        public virtual ICollection<Parts> Parts { get; set; }
    }
}
