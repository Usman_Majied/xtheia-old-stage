﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PatientConsent
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public string Ext { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string EncodedPath { get; set; }
        public int PhysicianPatientsId { get; set; }
        public string ThumbnailEncodedPath { get; set; }
        public int? ConsentId { get; set; }

        public virtual ConsentForm Consent { get; set; }
        public virtual PhysicianPatients PhysicianPatients { get; set; }
    }
}
