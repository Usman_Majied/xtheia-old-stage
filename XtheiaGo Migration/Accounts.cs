﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Accounts
    {
        public Accounts()
        {
            AspNetUsers = new HashSet<AspNetUsers>();
        }

        public int AccountId { get; set; }
        public string BusinessName { get; set; }
        public string Phone { get; set; }
        public string Street { get; set; }
        public string Suit { get; set; }
        public string City { get; set; }
        public int StateId { get; set; }
        public string ZipCode { get; set; }
        public int? CountryId { get; set; }
        public int AccountTypeId { get; set; }
        public sbyte? IsPreimum { get; set; }
        public string NameSecondary { get; set; }
        public string PhoneSecondary { get; set; }
        public string EmailSecondary { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? StatusId { get; set; }
        public int? TitleId { get; set; }
        public int? TitleIdSecondary { get; set; }
        public string NamePrimary { get; set; }
        public string EmailPrimary { get; set; }
        public string EmployeeNumber { get; set; }
        public string PhonePrimary { get; set; }
        public string Country { get; set; }

        public virtual AccountTypes AccountType { get; set; }
        public virtual Countries CountryNavigation { get; set; }
        public virtual States State { get; set; }
        public virtual StatusTypes Status { get; set; }
        public virtual Titles Title { get; set; }
        public virtual Titles TitleIdSecondaryNavigation { get; set; }
        public virtual ICollection<AspNetUsers> AspNetUsers { get; set; }
    }
}
