﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class ContentCatagories
    {
        public ContentCatagories()
        {
            Contents = new HashSet<Contents>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Contents> Contents { get; set; }
    }
}
