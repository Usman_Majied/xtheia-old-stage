﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Playlists
    {
        public Playlists()
        {
            DeployedPlaylists = new HashSet<DeployedPlaylists>();
            PlaylistContents = new HashSet<PlaylistContents>();
        }

        public int PlaylistId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public TimeSpan? Length { get; set; }
        public long? AssignedUserId { get; set; }
        public int? PlaylistTypeId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public int? RepeatCount { get; set; }

        public virtual AspNetUsers AssignedUser { get; set; }
        public virtual PlaylistTypes PlaylistType { get; set; }
        public virtual ICollection<DeployedPlaylists> DeployedPlaylists { get; set; }
        public virtual ICollection<PlaylistContents> PlaylistContents { get; set; }
    }
}
