﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PhysiciansPatientsContentsVersion
    {
        public int Id { get; set; }
        public int? PhysicianPatientsContentsId { get; set; }
        public string TextInImage { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public string ContentPath { get; set; }
        public string Notes { get; set; }
        public int PhysicialFileId { get; set; }

        public virtual PhysicialFiles PhysicialFile { get; set; }
        public virtual PhysiciansPatientsContents PhysicianPatientsContents { get; set; }
    }
}
