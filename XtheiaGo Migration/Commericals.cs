﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class Commericals
    {
        public int CommericalId { get; set; }
        public string Name { get; set; }
        public string RevisionNumber { get; set; }
        public string VenderName { get; set; }
        public int? VenderNumber { get; set; }
        public DateTime? CreatedDate { get; set; }
    }
}
