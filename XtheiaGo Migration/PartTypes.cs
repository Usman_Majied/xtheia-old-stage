﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PartTypes
    {
        public PartTypes()
        {
            Parts = new HashSet<Parts>();
        }

        public int Id { get; set; }
        public int PartCategoryId { get; set; }
        public string Name { get; set; }

        public virtual PartCategories PartCategory { get; set; }
        public virtual ICollection<Parts> Parts { get; set; }
    }
}
