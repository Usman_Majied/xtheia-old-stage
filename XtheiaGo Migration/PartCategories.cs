﻿using System;
using System.Collections.Generic;

namespace XtheiaGo_Migration
{
    public partial class PartCategories
    {
        public PartCategories()
        {
            PartTypes = new HashSet<PartTypes>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<PartTypes> PartTypes { get; set; }
    }
}
